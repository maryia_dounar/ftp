package com.epam.ftp;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import java.util.Scanner;
import java.io.*;


/**
 * Created by Maryia_Dounar on 5/14/2015.
 */
public class FtpAction {

   public void connect(FTPClient client) throws IOException{
        client.connect("ftp.mozilla.org");
        client.login("anonymous", "");
        int reply = client.getReplyCode();
        if(!FTPReply.isPositiveCompletion(reply)){
            System.out.println("Connect failed");
        }
    }
    public  void disconnect(FTPClient client) throws IOException{
        if (client.isConnected()) {
            client.logout();
            client.disconnect();
        }
    }
    public boolean isFileExist(FTPClient client, String fileName){
        try{
            FTPFile[] files = client.listFiles();
            for (FTPFile list : files){
                if(fileName.equals(list.getName())){
                    if(list.isFile()){
                        return true;
                    }
                }
            }

        }catch (IOException e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean isDirExist(FTPClient client, String dirName){
        try{
            FTPFile[] files = client.listFiles();
            for (FTPFile list : files){
                if(dirName.equals(list.getName())){
                    if(list.isDirectory()){
                        return true;
                    }
                }
            }

        }catch (IOException e){
            e.printStackTrace();
        }
        return false;
    }

    public void actionByName(FTPClient client, String command){
        try {
            if (isDirExist(client, command)) {
                client.changeWorkingDirectory(command);
                printWorkingDirectoryToConsole(client);
            } else if (isFileExist(client, command)) {
                downloadFile(client, command);
            } else {
                System.out.println("Undefine command: " + command);
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
    public  void printFiles(FTPClient client) throws IOException {
        FTPFile[] files = client.listFiles();
        for(int i = 0; i < files.length; i++) {
            System.out.println(files[i].getName());
        }
    }

    public void downloadFile(FTPClient client, String file) throws IOException {
        System.out.println("Do yoy want download file " + file + "? [Y|N)");
        Scanner sc = new Scanner(System.in);
        String ans = sc.nextLine();
        switch (ans) {
            case "Y":
                String path = System.getProperty("user.home") + "\\Downloads\\";
                File fileDown = new File(path + file);
                OutputStream os = new BufferedOutputStream(new FileOutputStream(fileDown));
                boolean fileDownloaded = client.retrieveFile(file, os);
                if (fileDownloaded) {
                    System.out.println("File has been downloaded");
                } else {
                    System.out.println("File hasn't been founded");
                }
                break;
            case "N":
                return;
            default:
                return;
        }
    }

    public void printWorkingDirectoryToConsole(FTPClient client) {
        try {
            System.out.println("Current dir: " + client.printWorkingDirectory());
        } catch (IOException e) {
            System.out.println("Unable to define current dir due to " + e.getMessage());
        }
    }

}
