package com.epam.ftp;

/**
 * Created by Maryia_Dounar on 5/13/2015.
 */
import org.apache.commons.net.ftp.*;
import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        FTPClient ftp = FTP.getInstance();
        FtpAction ftpAction = new FtpAction();
        try {
            ftpAction.connect(ftp);
                ftpAction.printWorkingDirectoryToConsole(ftp);
                ftpAction.printFiles(ftp);
                while (true) {
                    System.out.println("Please, type command. Available command:\n :up\n " +
                            ":print\n :exit\n :file or directory name");
                    String command = sc.nextLine();
                    switch (command){
                        case "up":
                            ftp.changeToParentDirectory();
                            ftpAction.printWorkingDirectoryToConsole(ftp);
                            break;
                        case "print":
                            ftpAction.printFiles(ftp);
                            break;
                        case "exit":
                            System.exit(0);
                            break;
                        default:
                            ftpAction.actionByName(ftp, command);
                            break;
                    }
                }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                ftpAction.disconnect(ftp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
