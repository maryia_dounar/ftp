package com.epam.ftp;

import org.apache.commons.net.ftp.FTPClient;

/**
 * Created by Maryia_Dounar on 5/14/2015.
 */
public class FTP {
    private static FTPClient instance;

    public static FTPClient getInstance(){
        if(instance == null){
            instance = new FTPClient();
        }
        return instance;
    }

}
